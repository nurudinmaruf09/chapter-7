const passport = require("passport")
const { Strategy: LocalStrategy } = require("passport-local")

const userDate = {
    username: "deen",
    fullname: "Administrator",
}

const validator = (username, password, done) => {
    if (username == "deen" && password =="123456") {
        return done(null, userData)
        }

    done(new Error("invaliid user"), false)
}

const strategy = new LocalStrategy({
    usernamefield: "username",
    passwordField: "password"
}, validator)

passport.use(strategy)

passport.serializeUser((user, done) => done(null, user.username))
passport.deserializeUser((username, done) => {
    if (username == "deen") {
        return done(null, userData)
    }

    done(new Error("unknown user", false))
})

module.exports = passport