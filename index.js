const express = require("express")
const app = express()
const session = require("express-session")
const FileStore = requre("express-file-store")(session)
const flash = require("express-flash")
const { PORT = 8080 } = process.env
const passport = require("./lib/passport")

// app.use(express.urlencoded({extended: false}))
app.use(express.json())
app.set("view engine", "ejs")

app.use(session({
    secret: "s3cr3t",
    saveUnitialized: false,
    resave: false,
    // store: new FileStore(),
}))
app.use(passport.initialize())
app.use(passport.session)


const router = require("./router")
app.use(router)

app.listen(PORT, () => {
    console.log(`server running on port ${PORT}`)
})