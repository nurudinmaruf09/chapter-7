const router = require("express").Router()
const home = require("../controllers/userController")
const authenticator = require("./lib/authenticator")

// .. define router
router.get("/profile", authenticator, user.profile)
router.get("/logout", auth.logout)
router.use((err, req, res, next) => {
    if (err) {
        return res.send(err.message)
    }

    next()
})

module.exports = router