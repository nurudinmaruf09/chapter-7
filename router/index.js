const router = require("express").Router()
const home = require("./home")

// .. define router
router.use("/", home)

module.exports = router