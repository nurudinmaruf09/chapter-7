const router = require("express").Router()
const home = require("../controllers/authController")
const authenticator = require("./lib/authenticator")

// .. define router
router.get("/login", auth.login)
router.post("/login", auth.loginPost)
router.use((err, req, res, next) => {
    if (err) {
        return res.send(err.message)
    }

    next()
})

module.exports = router