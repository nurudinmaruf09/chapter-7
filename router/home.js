const router = require("express").Router()
const home = require("../controllers/homeController")

// .. define router
router.get("/", home.index)

module.exports = router