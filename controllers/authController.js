const passport = require("passport")

module.exports = {
    login: (req, res) => {
        console.log(req.session.user)
        res.sender("login")
    },
    loginPost: passport.authenticate("local", {
        successRedirect: "/profile",
        failureRedirect: "/login",
        failureFlash: true,
    }),
    logout: (req, res) => {
        req.logout(err => {
            if (err) return next()
            res.redirect("/login")
        })
        req.session.destroy()
        res.redirect("/login")
    }
}