module.exports = {
    profile: (req, res) => {
        let params = {
            username: req.session.user?.username
        }

        res.render("profile", params)
    }
}